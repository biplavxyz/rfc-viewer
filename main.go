package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"

	"code.rocketnine.space/tslocum/cview"
	"github.com/gdamore/tcell/v2"
)

func main() {

	// Defining program arguments and validation
	if len(os.Args) != 2 {
		fmt.Println("Invalid number of arguments.")
		fmt.Println("First argument must be a rfc number.")
		os.Exit(1)
	}
	rfc_num := os.Args[1]

	// Using regexp to validate if the given argument is a number
	var isDigit = regexp.MustCompile(`^[0-9]+$`)
	bVar := isDigit.MatchString(rfc_num)

	if !bVar {
		fmt.Println("First argument must be a rfc number")
		os.Exit(2)
	}

	// Target Url
	URL := "https://www.ietf.org/rfc/rfc" + rfc_num + ".txt"

	// Saving Response
	resp, err := http.Get(URL)
	if err != nil {
		log.Fatal(err)
	}

	// Close connection later
	defer resp.Body.Close()

	// Read all of the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Converting the bytes of response body to string
	res := string(body)

	// Sets and runs cview TUI interface

	// Initialize application
	app := cview.NewApplication()

	tv := cview.NewTextView()

	// Set Border, Color, and Alignment
	tv.SetBorder(true)
	tv.SetBorderColor(tcell.ColorDarkCyan)
	tv.SetTextColor(tcell.ColorDarkOliveGreen)
	tv.SetTextAlign(cview.AlignCenter)

	// Set Title, and Color
	tv.SetTitle("RFC Viewer")
	tv.SetTitleColor(tcell.ColorDarkOrange)

	// Set Main Text
	tv.SetText(res)

	// Set the tv as root and focus on it
	app.SetRoot(tv, true)

	// Run the application
	if err := app.Run(); err != nil {
		panic(err)
	}

}
