module rfc-viewer

go 1.17

require (
	code.rocketnine.space/tslocum/cview v1.5.7
	github.com/gdamore/tcell/v2 v2.4.1-0.20210828201608-73703f7ed490
)

require (
	code.rocketnine.space/tslocum/cbind v0.1.5 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.14-0.20210830053702-dc8fe66265af // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.0.0-20210831042530-f4d43177bf5e // indirect
	golang.org/x/term v0.0.0-20210615171337-6886f2dfbf5b // indirect
	golang.org/x/text v0.3.7 // indirect
)
