# Rfc Viewer

A simple golang based TUI application that shows RFC details, provided the RFC number.

# Usage
`go run main.go rfc_number`

# Example
`go run main.go 2549`

![Demo](rfc-viewer.gif)
